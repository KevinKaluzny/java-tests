package value;

public class ValueType {
    public static final String STRING = "String";
    public static final String INTEGER = "Integer";
    public static final String FORMULA = "Formula";
}
