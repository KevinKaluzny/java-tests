package office;

import spreadsheet.SpreadsheetImpl;

import java.lang.reflect.Array;

public class Office implements SpreadsheetImpl {
    public String[][] cells;

    public Office(int rows, int columns) {
        cells = new String[rows][columns];
    }

    public static SpreadsheetImpl newSpreadsheet(int rows, int columns) {
        Office office = new Office(rows, columns);
        SpreadsheetImpl spreadsheet = office;
        return spreadsheet;
    }

    public String get(int row, int column) {
        if (cells[row][column] == null) {
            return "";
        }
        
        return cells[row][column];
    }

    public void put(int row, int column, String text) {
        for (int i = 0; i < 10; i++) {
            String number = Integer.toString(i);

            if (text.contains(number)) {
                text = text.trim();
            }
        }

        cells[row][column] = text;
    }

    public Object getValueType(int row, int column) {
        String value = cells[row][column];

        if (value.substring(0, 1).equals("=")) {
            return "Formula";
        }
        else {
            for (int i = 0; i < value.length() - 1; i++) {
                System.out.println(value.substring(i, i++));
                if (Character.isLetter(value.substring(i, ++i).toCharArray()[0])) {
                    return "String";
                }
            }

            return "Integer";
        }
    }

    public String[][] getCells() {
        return cells;
    }
}