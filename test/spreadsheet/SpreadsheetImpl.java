package spreadsheet;

public interface SpreadsheetImpl {
    String get(int row, int column);

    void put(int row, int column, String index);


    Object getValueType(int row, int column);

    String[][] getCells();
}
