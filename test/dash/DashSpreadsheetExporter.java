package dash;

import spreadsheet.SpreadsheetImpl;

public class DashSpreadsheetExporter {
    SpreadsheetImpl sheet;

    public String dashSpreadsheet = "10,5#" // Line breaks added for readability. There are no "\n" in the String
            + "-----" // 0
            + "-----" // 1
            + "-----" // 2
            + "-----" // 3
            + "-----" // 4
            + "-----" // 5
            + "-----" // 6
            + "-----" // 7
            + "-----" // 8
            + "-----"; // 9

    public DashSpreadsheetExporter(SpreadsheetImpl sheet) {
        this.sheet = sheet;
    }

    public String export() {
        String cell;

        for (int i = 9; i >= 0; i--) {
            for (int j = 4; j >= 0; j--) {
                try {
                    cell = sheet.get(i, j);
                } catch (IndexOutOfBoundsException exception) {
                    continue;
                }

                dashSpreadsheet = dashSpreadsheet.substring(0, 5 * i + j + 5) + cell + dashSpreadsheet.substring(5 * i + j + 5, dashSpreadsheet.length());
            }
        }

        return dashSpreadsheet;
    }
}
